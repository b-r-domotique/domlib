
TYPE
	WeatherMain_typ : 	STRUCT 
		temp : REAL;
		pressure : REAL;
		humidity : REAL;
		temp_min : REAL;
		temp_max : REAL;
	END_STRUCT;
	WeatherSys_typ : 	STRUCT 
		type : UDINT;
		id : UDINT;
		message : REAL;
		country : STRING[10];
		sunrise : DATE_AND_TIME;
		sunset : DATE_AND_TIME;
	END_STRUCT;
	WeatherClouds_typ : 	STRUCT 
		all : UDINT;
	END_STRUCT;
	WeatherWind_typ : 	STRUCT 
		speed : REAL;
		deg : REAL;
		degTransform : STRING[100];
	END_STRUCT;
	WeatherWeather_typ : 	STRUCT 
		id : UDINT;
		main : STRING[100];
		description : STRING[100];
		icon : STRING[10];
		iconPath : STRING[100];
	END_STRUCT;
	WeatherCoord_typ : 	STRUCT 
		lon : REAL;
		lat : REAL;
	END_STRUCT;
	WeatherSettings_typ : 	STRUCT 
		apiKey : STRING[100] := '621b1e1affc3225ea725dc7688a518b8';
		city : STRING[100] := 'Bouvron,fr';
		lang : STRING[10] := 'fr';
		units : STRING[10] := 'metric';
	END_STRUCT;
	Weather_typ : 	STRUCT 
		coord : WeatherCoord_typ;
		weather : WeatherWeather_typ;
		base : STRING[100];
		main : WeatherMain_typ;
		visibility : UDINT;
		wind : WeatherWind_typ;
		clouds : WeatherClouds_typ;
		dt : DATE_AND_TIME;
		sys : WeatherSys_typ;
		timezone : UDINT;
		id : UDINT;
		name : STRING[100];
		cod : UDINT;
		refresh : BOOL;
		refreshing : STRING[1000];
		settings : WeatherSettings_typ;
		message : STRING[1024];
	END_STRUCT;
END_TYPE
