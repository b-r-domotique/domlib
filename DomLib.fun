
{REDUND_ERROR} FUNCTION_BLOCK DomWeather (*Gestion du widget température*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		Enable : BOOL;
		MpLinkCfg : REFERENCE TO MpComIdentType;
		MpLink : REFERENCE TO MpComIdentType;
		PVName : STRING[32];
	END_VAR
	VAR_OUTPUT
		Status : DINT;
	END_VAR
	VAR
		pWeather : REFERENCE TO Weather_typ;
		state : UINT;
		httpsClient_0 : httpsClient;
		uri : STRING[1024];
		requestData : STRING[1024];
		responseData : STRING[1024];
		responseConvert : STRING[1024];
		weatherResponse : STRING[1024];
		JSON_Parse_0 : JSON_Parse;
		strTemp : STRING[100];
		refreshTimeout : TON;
		httpUtf8ToString_0 : httpUtf8ToString;
		MpRecipeRegPar_0 : MpRecipeRegPar;
		cfgPVName : STRING[79];
		adrPv : UDINT;
		lenPv : UDINT;
		_settings : WeatherSettings_typ;
		_crcSettings : UDINT;
	END_VAR
END_FUNCTION_BLOCK
