
(* Gestion du widget temp�rature *)
FUNCTION_BLOCK DomWeather
	
	httpsClient_0();
	refreshTimeout();
	
	
	IF Enable THEN
		PV_xgetadr(ADR(PVName), ADR(adrPv), ADR(lenPv));
		pWeather ACCESS adrPv;
		
		// Build URI from settings
		uri := WEATHER_API_URI;
		// API_KEY
		brsstrcat(ADR(uri), ADR('appid='));
		brsstrcat(ADR(uri), ADR(pWeather.settings.apiKey));
		// Alarme d'API_KEY missing
		IF pWeather.settings.apiKey = '' THEN MpAlarmXSet(MpLink, 'WeatherApiKeyMissing');
		ELSE MpAlarmXReset(MpLink, 'WeatherApiKeyMissing'); END_IF
		
		// CITY
		brsstrcat(ADR(uri), ADR('&q='));
		brsstrcat(ADR(uri), ADR(pWeather.settings.city));
		// Alarme de CITY missing
		IF pWeather.settings.city = '' THEN MpAlarmXSet(MpLink, 'WeatherCityMissing');
		ELSE MpAlarmXReset(MpLink, 'WeatherCityMissing'); END_IF
		
		// LANG
		brsstrcat(ADR(uri), ADR('&lang='));
		brsstrcat(ADR(uri), ADR(pWeather.settings.lang));
		// Alarme de LANG missing
		IF pWeather.settings.lang = '' THEN MpAlarmXSet(MpLink, 'WeatherLangMissing');
		ELSE MpAlarmXReset(MpLink, 'WeatherLangMissing'); END_IF
		
		// UNITS
		brsstrcat(ADR(uri), ADR('&units='));
		brsstrcat(ADR(uri), ADR(pWeather.settings.units));
		// Alarme de UNITS missing
		IF pWeather.settings.units = '' THEN MpAlarmXSet(MpLink, 'WeatherUnitsMissing');
		ELSE MpAlarmXReset(MpLink, 'WeatherUnitsMissing'); END_IF
		
		// API calls errors
		CASE pWeather.cod OF
			0, 200: // No call no error // Response OK
				MpAlarmXReset(MpLink, 'WeatherApiCallError');
			ELSE // 401, 404, 500 -> Api calls errors
				MpAlarmXSet(MpLink, 'WeatherApiCallError');
		END_CASE
		
		httpsClient_0.pHost 			:= ADR(WEATHER_API_HOST);
		httpsClient_0.pUri				:= ADR(uri);
		httpsClient_0.method			:= httpMETHOD_GET;
	
		httpsClient_0.pRequestData 		:= ADR(requestData);
		httpsClient_0.pResponseData 	:= ADR(responseData);
		httpsClient_0.requestDataLen 	:= brsstrlen(ADR(requestData));
		httpsClient_0.responseDataSize 	:= SIZEOF(responseData);
		
		CASE state OF
		
			0: //Call weather API
				
				httpsClient_0.enable 				:= TRUE;
				httpsClient_0.send 					:= TRUE;
				pWeather.refresh					:= FALSE;
				state := state + 1;

			1: // Wait response
				CASE httpsClient_0.httpStatus OF
					0: // Wait for server response
					
					ELSE
						httpsClient_0.send 			:= FALSE;
						state := state + 1;
						// Error case -> jump to timeout
						// TODO: Raise alarm / user message
//						httpsClient_0.enable 				:= FALSE;
//						httpsClient_0.send 					:= FALSE;
//						state := 3;
				END_CASE
			2: // Decode response
				// Conversion UTF-8 Vers String -> Affichage correct des caract�res sp�ciaux
				httpUtf8ToString_0(enable := 1, pSrc := ADR(responseData), pDest := ADR(responseConvert), destSize := SIZEOF(responseConvert), pMappingTable := 0);
				// Ajout d'un objet root au json recu de l'API
				weatherResponse := '{"';
				brsstrcat(ADR(weatherResponse), ADR(PVName));
				brsstrcat(ADR(weatherResponse), ADR('": '));
				brsstrcat(ADR(weatherResponse), ADR(responseConvert));
				brsstrcat(ADR(weatherResponse), ADR('}'));
				// Reset PV before parse JSON
				_settings := pWeather.settings;
				brsmemset(ADR(pWeather), 0, SIZEOF(Weather_typ));
				pWeather.settings := _settings;
				// JSON parse -> PV
				JSON_Parse_0(pMessage := ADR(weatherResponse));
				CASE JSON_Parse_0.Status OF
					1: 
						state := state + 1;
						// Reset fub from last call
						brsmemset(ADR(JSON_Parse_0), 0, SIZEOF(JSON_Parse_0));
						// Cr�e le path de l'icone m�t�o en fonction du code
						pWeather.weather.iconPath := 'LibraryMedia/DomLibW/Weather/'; // Workaround pour afficher l'image !! Attention si changement de nom de la librairie
						IF pWeather.weather.icon = '' THEN pWeather.weather.icon := 'error'; END_IF
						brsstrcat(ADR(pWeather.weather.iconPath), ADR(pWeather.weather.icon));
						brsstrcat(ADR(pWeather.weather.iconPath), ADR('.svg'));
						// Cr�e le transfrom pour la rotation de la boussole
						pWeather.wind.degTransform := '[{"select":"#Compass","spin":[';
						brsftoa(pWeather.wind.deg, ADR(strTemp));
						brsstrcat(ADR(pWeather.wind.degTransform), ADR(strTemp));
						brsstrcat(ADR(pWeather.wind.degTransform), ADR(',15,15]}]'));
						// Mise � l'heure UTC + offset correspondant au fuseau horaire
						// Sunrise
						pWeather.sys.sunrise := UDINT_TO_DT(DT_TO_UDINT(pWeather.sys.sunrise) + pWeather.timezone);
						// Sunset
						pWeather.sys.sunset := UDINT_TO_DT(DT_TO_UDINT(pWeather.sys.sunset) + pWeather.timezone);
						// Set error message in description
						IF pWeather.cod <> 200 THEN pWeather.weather.description := pWeather.message; END_IF
				END_CASE
			3: // Wait for another call // 10 minutes timeout
				httpsClient_0.enable 				:= FALSE;
				refreshTimeout.IN 					:= TRUE;
				refreshTimeout.PT 					:= t#10m;
				
				// Leave also if settings change
				IF refreshTimeout.Q = TRUE  OR pWeather.refresh  OR crc32(ADR(pWeather.settings), SIZEOF(pWeather.settings)) <> _crcSettings THEN
					_crcSettings := crc32(ADR(pWeather.settings), SIZEOF(pWeather.settings));
					state 							:= 0;
					refreshTimeout.IN 				:= FALSE;
				END_IF
			
		END_CASE
	
		IF state <> 3 THEN
			// Animation de la roue de refresh
			pWeather.refreshing := '<animateTransform xlink:href="#Refresh" attributeName="transform" attributeType="XML" type="rotate" from="0 12 12" to="360 12 12" dur="1s" repeatCount="indefinite"/>';
		ELSE
			pWeather.refreshing := '';
		END_IF
		cfgPVName := '';
		brsstrcat(ADR(cfgPVName), ADR(PVName));
		brsstrcat(ADR(cfgPVName), ADR('.settings'));
		MpRecipeRegPar_0(MpLink := ADR(MpLinkCfg), Enable := TRUE, PVName := ADR(cfgPVName));
		
	ELSE
		MpRecipeRegPar_0(MpLink := ADR(MpLinkCfg), Enable := FALSE, PVName := ADR(cfgPVName));
		httpsClient_0.enable 				:= FALSE;
		httpsClient_0.send 					:= FALSE;
		uri 								:= '';
		requestData							:= '';
		responseData						:= '';
		responseConvert						:= '';
	END_IF
	
	
END_FUNCTION_BLOCK
